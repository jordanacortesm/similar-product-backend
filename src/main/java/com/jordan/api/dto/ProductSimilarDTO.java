package com.jordan.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
public class ProductSimilarDTO {
	private String id;
	private String name;
	private int price;
	private boolean availability;
	
	public ProductSimilarDTO() {
		
	}
}
