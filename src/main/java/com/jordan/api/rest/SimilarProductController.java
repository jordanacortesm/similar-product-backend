package com.jordan.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jordan.api.converters.SimilarProductConverter;
import com.jordan.api.validations.SimilarProductValidations;
import com.jordan.infraestructure.ports.primary.SimilarProductService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/product")
@AllArgsConstructor
public class SimilarProductController {
	
	@Autowired
	private SimilarProductService similarProductService;
	@Autowired
	private SimilarProductValidations similarProductValidations;
	
	private SimilarProductConverter converterSimilarProduct;


	@GetMapping("/{productId}/similar")
	public ResponseEntity<?> similar(@PathVariable(value = "productId") String productId) {			
		if(similarProductValidations.validateField(productId)) {
			return new ResponseEntity<>(similarProductService.similar(converterSimilarProduct.convertStringToDTO(productId)),HttpStatus.OK);			
		}
		return new ResponseEntity<>("Product Not found",HttpStatus.NOT_FOUND);
	}
}
