package com.jordan.api.validations;

import org.springframework.stereotype.Component;

@Component
public class SimilarProductValidations {

	
	public boolean validateField(String productId) {
		return null != productId && !productId.isEmpty(); 
	}
	
}
