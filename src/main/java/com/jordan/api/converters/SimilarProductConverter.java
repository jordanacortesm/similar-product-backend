package com.jordan.api.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.jordan.api.dto.ProductSimilarDTO;
import com.jordan.infraestructure.domain.Product;

@Component
public class SimilarProductConverter {

	public ProductSimilarDTO convertStringToDTO(String productId) {
		return ProductSimilarDTO.builder()
		.id(productId)
		.build();
	}
	
	public Product convertDtoTODomain(ProductSimilarDTO productSimilarDTO) {
		return Product.builder().id(productSimilarDTO.getId()).build();
	}

	public List<ProductSimilarDTO> convertDomainToDTO(List<Product> product) {
		List<ProductSimilarDTO> productSimilarDTOList = new ArrayList<>();
		if (null != product && !product.isEmpty() && product.size() > 0) {
			product.stream().forEach(p -> {				
				productSimilarDTOList.add(ProductSimilarDTO.builder()
						.id(p.getId())
						.name(p.getName())
						.price(p.getPrice())
						.availability(p.isAvailability())
						.build());
			});
			return productSimilarDTOList;
		}
		return null;
	}
}
