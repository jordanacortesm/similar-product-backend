package com.jordan.infraestructure.ports.primary;

import org.springframework.http.ResponseEntity;

import com.jordan.api.dto.ProductSimilarDTO;

public interface SimilarProductService {
	
	public ResponseEntity<?> similar(ProductSimilarDTO productDTO);

}
