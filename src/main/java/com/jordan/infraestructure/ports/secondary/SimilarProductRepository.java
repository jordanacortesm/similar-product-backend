package com.jordan.infraestructure.ports.secondary;

import com.jordan.infraestructure.domain.Product;

public interface SimilarProductRepository {

	String[] findProductIdSimilar(Product product) throws Exception;
	
	Product getProductDetail(String productId)throws Exception;

}
