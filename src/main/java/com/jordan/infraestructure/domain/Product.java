package com.jordan.infraestructure.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Product {
	private String id;
	private String name;
	private int price;
	private boolean availability;
	
}
