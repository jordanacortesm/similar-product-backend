package com.jordan.infraestructure.adapters.secondary;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.jordan.infraestructure.domain.Product;
import com.jordan.infraestructure.ports.secondary.SimilarProductRepository;

@Repository
public class SimilarProductRepositoryImpl implements SimilarProductRepository {

	@Value("${endpoint.external.similarproduct}")
	private String urlBase;
	
	@Override
	public String[] findProductIdSimilar(Product product) throws Exception { 
        RestTemplate plantilla = new RestTemplate();
        return plantilla.getForObject(urlBase+"{productId}/similarids", String[].class,loadMapParamsForExternalServices(product.getId()));
    }

	@Override
	public Product getProductDetail(String productId) throws Exception {
		 RestTemplate plantilla = new RestTemplate();
	     return plantilla.getForObject(urlBase+"{productId}/", Product.class,loadMapParamsForExternalServices(productId));
	}
	
	private Map<String, String> loadMapParamsForExternalServices(String productId) {
		Map<String, String> params = new HashMap<String, String>();
        params.put("productId", productId);
		return params;
	}
}
