package com.jordan.infraestructure.adapters.primary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jordan.api.converters.SimilarProductConverter;
import com.jordan.api.dto.ProductSimilarDTO;
import com.jordan.infraestructure.domain.Product;
import com.jordan.infraestructure.ports.primary.SimilarProductService;
import com.jordan.infraestructure.ports.secondary.SimilarProductRepository;

@Service
public class SimilarProductServiceImpl implements SimilarProductService {

	@Autowired
	private SimilarProductRepository similarProductRepostirory;
	@Autowired
	private SimilarProductConverter similarProductConverter;

	@Override
	public ResponseEntity<?> similar(ProductSimilarDTO productDTO) {
		List<String> ids;
		try {
			ids = findProductSimilar(productDTO);
			if (null != ids && !ids.isEmpty() && ids.size() > 0) {
				return getProductDetail(ids);
			} else {
				return returnNotFound();
			}
		} catch (Exception e) {
			return returnNotFound();
		}
	}

	private List<String> findProductSimilar(ProductSimilarDTO productDTO) throws Exception {
		String[] ids = null;
		ids = similarProductRepostirory.findProductIdSimilar(similarProductConverter.convertDtoTODomain(productDTO));
		List<String> listSimilarIds = Arrays.asList(ids);
		return listSimilarIds;
	}

	private ResponseEntity<?> getProductDetail(List<String> similarProductsIds) throws Exception {
		List<Product> productDetail = new ArrayList<>();
		similarProductsIds.stream().forEach(id ->{
			try {
				productDetail.add(similarProductRepostirory.getProductDetail(id));
			} catch (Exception e) {
				System.out.print(e);
			}
		});
		return new ResponseEntity<List<ProductSimilarDTO>>(similarProductConverter.convertDomainToDTO(productDetail), HttpStatus.OK);
	}
	
	private ResponseEntity<?> returnNotFound() {
		return new ResponseEntity<String>("Product not Found", HttpStatus.NOT_FOUND);
	}	
	
}	
