package com.jordan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimilarProductBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimilarProductBackendApplication.class, args);
	}

}
